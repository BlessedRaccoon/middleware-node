import { existsSync } from 'fs';
import { logerror } from "blessed-logcolor";

/**
 * Function that loads a page
 * @param {Promise[]} promises Array of promises of postgreSQL queries
 * @param {string[]} names Array of String linked to each promise
 * @param {object} others Other parameters
 * @param {string} page Page to load
 * @param {Request} req req
 * @param {Response} res res
 */
async function loadpage(promises: Array<any> = [], names: string[] = [], param: any = {}, page: string = "index.pug", req: any, res: any) {
    param.USER = req.session.user;
    param.VERSION = req.session.changelog;
    param.existsSync = existsSync;
    param.logerror = logerror;
    if (promises.length !== 0){
        let values = await Promise.all(promises);
        for(let i=0; i<names.length ; i++) {
            param[names[i]] = values[i];
        }
    }
    res.render(page, param);
}

export {
    loadpage
}