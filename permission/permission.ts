/**
 * Function that redirects if the user has not the necessary grade
 * @param {*} req req
 * @param {*} res res
 * @param {string} redirection page to redirect
 * @param {number} grade minimum grade
 * @returns {boolean} true if the user needs to be redirected
 */
async function redirect(req: any, res: any, redirection: string = "/", grade: number = 1): Promise<boolean>{
    if(! isConnected(req)) {
        await res.redirect(redirection);
        return true;
    }
    if(verifyGrade(req.session.user.role, grade)) {
        await res.redirect(redirection);
        return true;
    }
    return false;
}

/**
 * Function that tells if the user is connected
 * @param {*} req req
 * @returns {boolean} true if the user is not connected
 */
function isConnected(req: any): boolean {
    return (req.session.user !== undefined);
}

/**
 * Returns true if the grade is below the neededGrade
 * @param {number} grade grade to compare
 * @param {number} neededGrade minimum grade to have
 * @returns {boolean} true if the grade is not high enough
 */
function verifyGrade(grade: number, neededGrade: number): boolean {
    return (grade < neededGrade);
}

export {
    redirect
}